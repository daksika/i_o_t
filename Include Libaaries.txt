// Include Libraries
#include "Arduino.h"
#include "DCMDriverL298.h"
#include "Adafruit_NeoPixel.h"
#include "PIR.h"
#include "SevenSegment.h"
#include "PiezoSpeaker.h"


// Pin Definitions
#define DCMOTORDRIVERL298_PIN_INT1	0
#define DCMOTORDRIVERL298_PIN_ENB	14
#define DCMOTORDRIVERL298_PIN_INT2	2
#define DCMOTORDRIVERL298_PIN_ENA	4
#define DCMOTORDRIVERL298_PIN_INT3	12
#define DCMOTORDRIVERL298_PIN_INT4	13
#define LEDRGB_PIN_DIN	15
#define PIR_PIN_SIG	16
#define S7S_PIN_RX	5
#define THINSPEAKER_PIN_POS	9



// Global variables and defines
#define LedRGB_NUMOFLEDS 1
unsigned int s7scounter = 0;  // This variable will count up to 65k
unsigned int thinSpeakerHoorayLength          = 6;                                                      // amount of notes in melody
unsigned int thinSpeakerHoorayMelody[]        = {NOTE_C4, NOTE_E4, NOTE_G4, NOTE_C5, NOTE_G4, NOTE_C5}; // list of notes. List length must match HoorayLength!
unsigned int thinSpeakerHoorayNoteDurations[] = {8      , 8      , 8      , 4      , 8      , 4      }; // note durations; 4 = quarter note, 8 = eighth note, etc. List length must match HoorayLength!
// object initialization
DCMDriverL298 dcMotorDriverL298(DCMOTORDRIVERL298_PIN_ENA,DCMOTORDRIVERL298_PIN_INT1,DCMOTORDRIVERL298_PIN_INT2,DCMOTORDRIVERL298_PIN_ENB,DCMOTORDRIVERL298_PIN_INT3,DCMOTORDRIVERL298_PIN_INT4);
Adafruit_NeoPixel LedRGB(LEDRGB_PIN_DIN);
PIR pir(PIR_PIN_SIG);
S7S s7s(S7S_PIN_RX);
PiezoSpeaker thinSpeaker(THINSPEAKER_PIN_POS);


// define vars for testing menu
const int timeout = 10000;       //define timeout of 10 sec
char menuOption = 0;
long time0;

// Setup the essentials for your circuit to work. It runs first every time your circuit is powered with electricity.
void setup() 
{
    // Setup Serial which is useful for debugging
    // Use the Serial Monitor to view printed messages
    Serial.begin(9600);
    while (!Serial) ; // wait for serial port to connect. Needed for native USB
    Serial.println("start");
    
    LedRGB.begin(); // This initializes the NeoPixel library.
    LedRGB.show(); // Initialize all leds to 'off'
    s7s.clearDisplay();  // Clears display, resets cursor
    s7s.writeStr("-HI-");  // Displays -HI- on all digits   
    s7s.setBrightness(255);  // High brightness
    menuOption = menu();
    
}

// Main logic of your circuit. It defines the interaction between the components you selected. After setup, it runs over and over again, in an eternal loop.
void loop() 
{
    
    
    if(menuOption == '1') {
    // L298N Motor Driver with Dual Standard DC Motors (Geared) - Test Code
    //Start both motors. note that rotation direction is determined by the motors connection to the driver.
    //You can change the speed by setting a value between 0-255, and set the direction by changing between 1 and 0.
    dcMotorDriverL298.setMotorA(200,1);
    dcMotorDriverL298.setMotorB(200,0);
    delay(2000);
    //Stop both motors
    dcMotorDriverL298.stopMotors();
    delay(2000);

    }
    else if(menuOption == '2') {
    // LED - RGB Addressable, PTH, 5mm Diffused (5 Pack) - Test Code
    for(int i=0 ; i <= LedRGB_NUMOFLEDS ; i++){
    for (int k = 0 ; k <= 255 ; k++) {
    // set leds Color to RGB values, from 0,0,0 up to 255,255,255
    LedRGB.setPixelColor(i, LedRGB.Color(255-k,k,100)); // turn on green color on led #i.
    if (i > 0)
    LedRGB.setPixelColor(i-1, LedRGB.Color(0,0,0)); // turn off last led
    LedRGB.show(); //update led color to the hardware.
    delay(1);
    }
    }

    }
    else if(menuOption == '3') {
    // Infrared PIR Motion Sensor Module - Test Code
    bool pirVal = pir.read();
    Serial.print(F("Val: ")); Serial.println(pirVal);

    }
    else if(menuOption == '4') {
    // 7 - Segment Serial Display - Test Code
    // This will output the counter value to the display
    s7s.writeInt(s7scounter);
    s7scounter++;  // Increment the counter
    delay(50);  
    }
    else if(menuOption == '5') {
    // Thin Speaker - Test Code
    // The Speaker will play the Hooray tune
    thinSpeaker.playMelody(thinSpeakerHoorayLength, thinSpeakerHoorayMelody, thinSpeakerHoorayNoteDurations); 
    delay(500);   
    }
    
    if (millis() - time0 > timeout)
    {
        menuOption = menu();
    }
    
}



// Menu function for selecting the components to be tested
// Follow serial monitor for instrcutions
char menu()
{

    Serial.println(F("\nWhich component would you like to test?"));
    Serial.println(F("(1) L298N Motor Driver with Dual Standard DC Motors (Geared)"));
    Serial.println(F("(2) LED - RGB Addressable, PTH, 5mm Diffused (5 Pack)"));
    Serial.println(F("(3) Infrared PIR Motion Sensor Module"));
    Serial.println(F("(4) 7 - Segment Serial Display"));
    Serial.println(F("(5) Thin Speaker"));
    Serial.println(F("(menu) send anything else or press on board reset button\n"));
    while (!Serial.available());

    // Read data from serial monitor if received
    while (Serial.available()) 
    {
        char c = Serial.read();
        if (isAlphaNumeric(c)) 
        {   
            
            if(c == '1') 
    			Serial.println(F("Now Testing L298N Motor Driver with Dual Standard DC Motors (Geared)"));
    		else if(c == '2') 
    			Serial.println(F("Now Testing LED - RGB Addressable, PTH, 5mm Diffused (5 Pack)"));
    		else if(c == '3') 
    			Serial.println(F("Now Testing Infrared PIR Motion Sensor Module"));
    		else if(c == '4') 
    			Serial.println(F("Now Testing 7 - Segment Serial Display"));
    		else if(c == '5') 
    			Serial.println(F("Now Testing Thin Speaker"));
            else
            {
                Serial.println(F("illegal input!"));
                return 0;
            }
            time0 = millis();
            return c;
        }
    }
}
